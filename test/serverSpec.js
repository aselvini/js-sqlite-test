/*eslint-env es6, node, mocha */

import coMocha from 'co-mocha';
import coSupertest from 'co-supertest';
import supertest from 'supertest';
import app from '../app/server';
import chai from 'chai';
import http  from 'http';

var expect = chai.expect;

describe('Given koa server', function () {
    beforeEach(function() {
        this.app = app.listen();
        this.request = supertest.agent(this.app);
    });

    afterEach(function() {
        this.app.close();
    });

    it('get / url', function *() {
        var res = yield this.request.get('/')
            .expect('Hello world!')
            .expect(200)
            .end();
        expect(res.text).to.equal('Hello world!');
    });
});
