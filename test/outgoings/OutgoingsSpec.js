import fs from 'fs';
import chai from 'chai';
import sqlite from 'sqlite3';
import Outgoings from '../../app/outgoings/Outgoings';

var expect = chai.expect;

describe('Outgoings api:', function() {
    before(function() {
        this.dbFileName = 'dummy.db';
        this.tableName = 'outgoings';
        this.testRecords = [
            { product: 'Prod A'
            , price: 12.34
            , date: new Date().getTime()
            , tag: '#uno, #due' 
            },
            { product: 'Prod B'
            , price: 98.76
            , date: new Date().getTime()
            , tag: '#due, #tre' 
            }];
    });

    after(function() {
        if (this.outgoings.open) {    
            this.outgoings.close();
        }
        if (fs.existsSync(this.dbFileName)) {
            fs.unlink(this.dbFileName);
        }
    });

    it(`creates a database`, function() {
        this.outgoings = new Outgoings();

        return this.outgoings.create(this.dbFileName)
            .then( () => countTablesByName(this.dbFileName, this.tableName) )
            .then( (tableCount) => expect(tableCount).to.equal(1) );
    });

    it('adds outgoing', function() {
        return this.outgoings.add(this.testRecords)
            .then( () => getFirstRecordInTable(this.dbFileName, this.tableName) )
            .then( (record) => expect( record.date ).to.equal(this.testRecords[0].date) );
    });

    it('modifies outoging', function() {
        let newPrice = 456;

        return getFirstRecordInTable(this.dbFileName, this.tableName)
            .then( (record) => this.outgoings.modify([
                                { id: record.id
                                , price: newPrice
                                }]) )
            .then( (id) => getRecordInTableById(this.dbFileName, this.tableName, id) )
            .then( (record) => expect( record.price ).to.equal(newPrice) );
    });

    it('removes outgoing', function() {
        return getFirstRecordInTable(this.dbFileName, this.tableName)
            .then((row) => this.outgoings.remove(row.id) )
            .then( (id) => getRecordInTableById(this.dbFileName, this.tableName, id) )
            .then( (row) => expect( undefined ).to.equal(row) );
    });

    function countTablesByName(dbName, tableName) {
        let db = new sqlite.Database(dbName, sqlite.OPEN_READONLY);
        let query = `SELECT count(*) as tables FROM sqlite_master WHERE type="table" AND name="${tableName}";`;

        return new Promise((resolve, reject) => {
            db.get(query, (err, row) => { resolve(err ? -1 : row.tables);
                                          db.close();
                                        });
            });
    }

    function getFirstRecordInTable(dbName, tableName) {
        let db = new sqlite.Database(dbName, sqlite.OPEN_READONLY);
        return new Promise( (resolve, reject) => {
            db.get(`SELECT * FROM ${tableName}`, (err, row)=> {
                    resolve(row);
            });
        });
    }

    function getRecordInTableById(dbName, tableName, id) {
        let db = new sqlite.Database(dbName, sqlite.OPEN_READONLY);
        sqlite.verbose();
        return new Promise( (resolve, reject) => {
            db.get(`SELECT * FROM ${tableName} WHERE ID in (${id})`, 
                    (err, record)=> !err ? resolve(record) : reject(err) );
        });
    }
});
