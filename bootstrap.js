require('babel/register');
var app = require('./app/server.js');

if (!module.parent) {
    app.listen(2501);
    console.log('Listen on 2501');
}
