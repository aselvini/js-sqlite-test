import sqlite from 'sqlite3';
import fs from 'fs';

'use strict';

class Outgoing {
    constructor(dbFileName) {
        this.filename = null;
        dbFileName && this.load(dbFileName);
    }

    load(dbFileName) {
        if (fs.existsSync(dbFileName)) {
            this.db = new sqlite.Database(dbFileName);
        }
    }

    create(dbFileName) {
        if (fs.existsSync(dbFileName)) {
            throw new Error(`File ${dbFileName} already exists`);
        }

        return new Promise((resolve, reject) => {
            this.db = new sqlite.Database(dbFileName);
            this.db.run(CREATE_DB, function(err) { 
                    err ? reject(err) : resolve(this.db);
                });

        }.bind(this));
    }

    serialize(func) {
        return this.db.serialize(func);
    };

    _dumpError(err) {
        err && console.log('Sqlite create err: ',err);
    }

    add(outgoings) {
        return new Promise( (resolve) => {
            var stmt = this.db.prepare(INSERT_OUTGOING);
            outgoings.map( (outgoing) => stmt.run([ outgoing.product, outgoing.price, outgoing.date, outgoing.tag]) );
            stmt.finalize(resolve);
        });
    }

    modify(outgoings) {
        var sql = 'BEGIN TRANSACTION;';
        sql += outgoings.map( (outgoing) => this._buildUpdateQuery(outgoing) ).join('');
        sql += 'END TRANSACTION;';

        return new Promise( (resolve, reject) => {
            var ids = outgoings.map( (outgoing) => outgoing.id );
            this.db.exec(sql, (error) => !error ?  resolve(ids) : reject(error) );
        });
    }

    remove(ids) {
        if (typeof(ids) === 'string') {
            ids = [ids];
        }
        return new Promise( (resolve, reject)=> {
            this.db.exec(this._buildRemoveQuery(ids), (error) => !error ? resolve(ids) : reject(error) );
        });
    }

    _buildUpdateQuery(outgoing) {
        var props = {};
        Object.assign(props, outgoing);

        var id = props.id;
        delete props.id;
        
        var fields = Object.keys(props).map((property) => property + '=' + props[ property ]);

        return `UPDATE outgoings SET ${fields} WHERE id=${id};`;
    }

    _buildRemoveQuery(ids) {
        var list = ids instanceof Array ? ids.join(',') : ids;
        return `DELETE FROM outgoings WHERE id in (${list});`;
    }

    getLast() {
        return this.db.get(SELECT_FIRST);
    }

    close() {
        this.db && this.db.close();
    }
}

const CREATE_DB = `CREATE TABLE outgoings
    ( id INTEGER PRIMARY KEY ASC
    , product TEXT 
    , price REAL DEFAULT 0
    , date INTEGER DEFAULT (datetime('now', 'localtime'))
    , tag TEXT
    );`;

const INSERT_OUTGOING = 'INSERT INTO outgoings(product, price, date, tag) VALUES(?, ?, ?, ?);';
const REMOVE_OUTGOING = 'DELETE FROM outgoings WHERE ID in ${list};';
const SELECT_FIRST = 'SELECT * FROM outgoings ORDERBY date LIMIT 1';

export default Outgoing;
