import koa from 'koa';
import krouter from 'koa-router';

var app = koa();
var router = krouter();

router.get('/', function *() {
    this.body = 'Hello world!';
});

app.use(router.routes());

export default app;
